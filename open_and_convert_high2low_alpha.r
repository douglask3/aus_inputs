configure_open_and_convert_high2low_alpha <- function() {

    library("raster")
    library("ncdf")
    library("RNetCDF")
    
    ## Input info
    filename_in         <<- "data/alpha_annual.nc"
    
    ## grid info
    lgrid               <<- raster(ncol=720, nrow=360)
    #lgrid=raster(nrows=180, ncols=70, xmn=110, ymn=-45)
    
    ##Output info
	filename_out        <<- "outputs/low_res_alpha.nc" #CV
    varname             <<- 'alpha'     #CV
    date_descp          <<- paste("date:", Sys.Date(), sep =" ")
    long_name           <<- "AET/PET" #CV
    unitss              <<- "-" #CV
    data_source         <<- "Australian annual average alpha, initially constructed in bitbuckey:-douglask3/resprouting_workship/bioclimatic_limits. Original data constructed using Bioclimate.f in resporting_workshop/bioclimatic_limits" #CV
    convert_descp       <<- "Regridded to a 0.5 degree grid and converted to netcdf in R by Doug Kelley"
    sript_name          <<- "configure_open_and_convert_high2low_alpha" #Doug 03/13: need to change so this is automated
    contact_descp       <<- "See bitbucket.org/douglask3/aus_inputs for scripts and contact information"
    
    ## local parameters
    nmonths             <<- 12
}


open_and_convert_high2low_alpha <-function() {
    configure_open_and_convert_high2low_alpha()

    
    #start things going, make raster from first file
    
    hdata3=raster(filename_in)
    #hdata3=aggregate(hdata3,5)
    ldata3=resample(hdata3,lgrid)
    
    # write  into netcdf	
	writeRaster(ldata3,filename_out,format="CDF",overwrite=TRUE)
    nc <- open.nc(filename_out, write=TRUE)
    
    #var.rename.nc(nc, 'alpha', varname)  
    
    att.put.nc(nc, varname, "long_name", "NC_CHAR", long_name)
    att.put.nc(nc, varname, "units", "NC_CHAR", unitss)
    att.put.nc(nc, varname, "data source", "NC_CHAR", data_source)
    att.put.nc(nc, varname, "conversion information", "NC_CHAR", convert_descp)
    att.put.nc(nc, varname, "script name", "NC_CHAR", sript_name)
    att.put.nc(nc, varname, "more information", "NC_CHAR", contact_descp)
    att.put.nc(nc, varname, "Date created", "NC_CHAR", date_descp)  
	close.nc(nc)
	
}
     
        