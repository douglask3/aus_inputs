library("gitProjectExtras")

sourceAllLibs()

filenameEg = "data/maxt/agrids/maxt_1972_03.asc"

filenameOut="low_res_rain_aus.nc"
varnameOut='pr'     #CV
longName="Monthly rainfall"
units="mm/month" #CV

open_and_convert_asc(filenameEg,filenameOut,varnameOut,longName,units,detrend=TRUE)