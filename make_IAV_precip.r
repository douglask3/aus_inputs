configure_miav <- function() {
    library("raster")
    library("ncdf")
    library("RNetCDF")
    
    filename_in     <<- 'hgh_res_rain_aus.nc'
    varname_in      <<- 'pr'
    nyears          <<- 30
    
    filename_out    <<- 'precip_IAV_ANU_aus.nc'
    varname_out     <<- 'IAV'
    
    long_name       <<-
        'precipitation inter-annual variablity'
    units_desc      <<- 'IAV / mean'
    time_period     <<- 'Jan 1970 -Dec 1999'
    data_source     <<- 'Australian rainfall from ANU climate asc files'
    convert_descp   <<- 'Precip initially converted into netcdf using open_and_convert_asc.r. Then onverted into ratio of IAV to mean precip'
    sript_name      <<- 'make_IAV_precip.r'
    contact_descp   <<- "See bitbucket.org/douglask3/aus_inputs for scripts and contact information"
    date_descp      <<- paste("date:", Sys.Date(), sep =" ")
     
}

make_IAV_precip <-function() {
    configure_miav()
    
    
    ## Open precip
    pr=brick(filename_in,varname=varname_in)
    
    ## Split precip into years
    startp=1
    for (i in 1:30) {        
        print(paste("year:",as.character(i)))
        adata=group_months_into_year(pr,startp)
        startp=startp+12
        
        if (i==1) { 
            apr=adata
        } else {
            apr=addLayer(apr,adata) 
        }
        
    }
    

    ##make std of years
    iav=make_IAV_of_layers(apr)
    
    
    ##output
    write_to_netcdf(iav)
    
    return(iav)
    
 }
 
group_months_into_year <- function(mdata,startp,countp=12) {
    
    adata=mdata[[startp]]
    
    for (m in 2:countp) {
        startp=startp+1
        adata=adata+mdata[[startp]]
    }
    
    return(adata)
}

make_IAV_of_layers <- function(ldata) {

    llayers=nlayers(ldata)
    ones=rep(1,llayers)
    lmean=stackApply(ldata,ones,'mean')
    ldelt=ldata-lmean
    ldelt=ldelt*ldelt
    
    lvarn=stackApply(ldelt,ones,'sum')/llayers
    
    lvarn=sqrt(lvarn)/lmean
    
}

write_to_netcdf <- function(ncdata) {
    writeRaster(ncdata,filename_out,format="CDF",overwrite=TRUE)
    nc <- open.nc(filename_out, write=TRUE)
    
    var.rename.nc(nc, 'layer', varname_out)  
    
    att.put.nc(nc, varname_out, "long_name", "NC_CHAR", long_name)
    att.put.nc(nc, varname_out, "units", "NC_CHAR", units_desc)
    att.put.nc(nc, varname_out, "data source", "NC_CHAR", data_source)
    att.put.nc(nc, varname_out, "time period", "NC_CHAR", time_period)
    att.put.nc(nc, varname_out, "conversion information", "NC_CHAR", convert_descp)
    att.put.nc(nc, varname_out, "script name", "NC_CHAR", sript_name)
    att.put.nc(nc, varname_out, "more information", "NC_CHAR", contact_descp)
    att.put.nc(nc, varname_out, "Date created", "NC_CHAR", date_descp)  
    
    close.nc(nc)
}

    
    