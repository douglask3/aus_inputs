detrendRaster <- function(r) {
    vs=values(r)
    test=!is.na(vs)
    test=apply(test,1,all)
    vs[test,]=t(apply(vs[test,],1,detrendCell))
    values(r)=vs
    return(r)
}
    
detrendCell <- function(vs) {
    x=1:length(vs)
    g=coefficients(lm(vs~x))[2]
    vs=vs-g*x
    return(vs)
}