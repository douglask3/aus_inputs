open_and_convert_asc_cfg <- function() {
    library("raster")
    library("ncdf")
    library("RNetCDF")
}

open_and_convert_asc <-function(filenameEg,
            filenameOut,varnameOut,longName,units,
            YrPosInStr = nchar(filenameEg) - c(10,7),
            MnPosInStr = nchar(filenameEg) - c(5,4),
            agFact=10,nyears=4,start_year=1970,
            doHighRes=FALSE,detrend=FALSE) {
            
    open_and_convert_asc_cfg()
    nmonths=12
    
    date_descp          =paste("date:", Sys.Date(), sep =" ")
    data_source         ="Australian rainfall from ANU climate asc files" #CV
    convert_descp_low   ="Regridded to a 0.5 degree grid and converted to netcdf in R by Doug Kelley"
    convert_descp_high  ="Converted to netcdf in R by Doug Kelley"
    sript_name          ="open_and_convert_asc" 
    contact_descp       ="See bitbucket.org/douglask3/aus_inputs for scripts and contact information"
    
    #start things going, make raster from first file
    hdata=raster(filenameEg)
    ldata=aggregate(hdata,fact=agFact)
    
    ptm <- proc.time()
    for (y in 1:nyears) {
        year=start_year+y-1
        substring(filenameEg,YrPosInStr[1],YrPosInStr[2]) = as.character(year)
        
        for (m in 1:nmonths) {
            if (m<10) {
                substring(filenameEg,MnPosInStr[1]) = as.character(0)
                substring(filenameEg,MnPosInStr[2]) = as.character(m)
            } else {
                substring(filenameEg,MnPosInStr[1],MnPosInStr[2]) = as.character(m)
            }
            print(filenameEg)
            
            hdata2=raster(filenameEg)     
            ldata2=aggregate(hdata2,fact=agFact)
            ldata =addLayer(ldata,ldata2)
            if (doHighRes) hdata =addLayer(hdata,hdata2)
        }
    }
    time_taken=proc.time() - ptm
	ldata=dropLayer(ldata,1)
	if (doHighRes)  hdata=dropLayer(hdata,1)
    
    if (detrend) {
        ldata=detrendRaster(ldata)
        if (doHighRes) hdata=detrendRaster(hdata)
    }

    # write low and high into netcdf	
	writeRaster(ldata,filename_out,format="CDF",overwrite=TRUE)
    nc <- open.nc(filename_out, write=TRUE)
    
    var.rename.nc(nc, 'variable', varnameOut)    
    var.rename.nc(nc, 'value', 'time')
    
    att.put.nc(nc, varnameOut, "longName", "NC_CHAR", longName)
    att.put.nc(nc, varnameOut, "units", "NC_CHAR", units)
    att.put.nc(nc, varnameOut, "data source", "NC_CHAR", data_source)
    att.put.nc(nc, varnameOut, "conversion information", "NC_CHAR", convert_descp_low)
    att.put.nc(nc, varnameOut, "script name", "NC_CHAR", sript_name)
    att.put.nc(nc, varnameOut, "more information", "NC_CHAR", contact_descp)
    att.put.nc(nc, varnameOut, "Date created", "NC_CHAR", date_descp)  
	
    att.put.nc(nc, "time", "longName", "NC_CHAR", "month") 
    att.put.nc(nc, "time", "description", "NC_CHAR", "months since 1970. month 1 = Jan 1970")    
	close.nc(nc)
    
    
    if (doHighRes) {
        substring(filename_out,1,6)="hgh_res"
        
        writeRaster(hdata,filename_out,format="CDF",overwrite=TRUE)
        nc <- open.nc(filename_out, write=TRUE) 
        
        var.rename.nc(nc, 'variable', varnameOut)     
        var.rename.nc(nc, 'value', 'time')
          
        att.put.nc(nc, varnameOut, "longName", "NC_CHAR", longName)
        att.put.nc(nc, varnameOut, "units", "NC_CHAR", units)
        att.put.nc(nc, varnameOut, "data source", "NC_CHAR", data_source)
        att.put.nc(nc, varnameOut, "conversion information", "NC_CHAR", convert_descp_high)
        att.put.nc(nc, varnameOut, "script name", "NC_CHAR", sript_name)
        att.put.nc(nc, varnameOut, "more information", "NC_CHAR", contact_descp)
        att.put.nc(nc, varnameOut, "Date created", "NC_CHAR", date_descp)  
        
        att.put.nc(nc, "time", "longName", "NC_CHAR", "month") 
        att.put.nc(nc, "time", "description", "NC_CHAR", "months since 1970. month 1 = Jan 1970")    
        close.nc(nc)
    }
}
     
        