configure_mseam <- function() {
    library("raster")
    library("ncdf")
    library("RNetCDF")
    
    
    mask_filename       <<-
        "C:/Users/mq42056055/Documents/Inputs/Historical/Ausmask_0k.nc"
    mask_varname        <<- 'mask'
    
    veg_filename        <<-
        "C:/Users/mq42056055/Documents/benchmark-system/bench_data/veg_cont_fields_CRU.nc"
    veg_varname         <<- "Tree_cover"
    veg_threshold       <<- 50
    recenter_veg        <<- 0
    
    lat_range           <<- c(-45,-25)
    lon_range           <<- c(135,155)
    
    write_new_file      <<- FALSE
    filename_out        <<- 'SE_Ausmask_0k.nc'
    
    data_description    <<- "Combined LPX Aus mask and tree cover > 50 percent between 25-45S and 135-155W. Tree cover from VCF. See benchmarking_system repo in douglask3 bitbucket account."
    sript_name          <<- "make_south_east_Aus_mask"
    contact_descp       <<- "Doug Kelley - douglas.kelley@students.mq.edu.au"
    date_descp          <<- paste("date:", Sys.Date(), sep =" ")
}

make_south_east_Aus_mask <-function() {
    configure_mseam()
    
    mask0=raster(mask_filename,varname=mask_varname)
    veg  =raster(veg_filename,varname=veg_varname)
    veg  =resample(veg,mask0)
    veg=recenter_ratser(veg,recenter_veg)
    
    veg=veg>veg_threshold
    mask0=mask0==1
    
    mask=((veg+mask0)==2)*1
    mask[mask==0]=NA
    
    mask=remove_outside_range(mask)
    
    
    if (write_new_file) {
        write_to_new_nc(mask)
    } else {
        file.copy(mask_filename,filename_out,overwrite=TRUE)
        nc=open.nc(filename_out,write=TRUE)
        var.put.nc(nc,"mask",t(as.matrix(mask)[360:1,]))
        close.nc(nc)
    }
   
}

recenter_ratser <- function(rast,ID) {
    
    if (ID==0) {
        return(rast)
    }
    a=dim(rast)[ID]
    if (ID==1) {
        rasti=rast
        rast[1:ceiling(a/2),]=rasti[(floor(a/2)+1):a,]
        rast[(ceiling(a/2)+1):a,]=rasti[1:floor(a/2),]
    } else {
        rasti=rast
        rast[,1:ceiling(a/2)]=rasti[,(floor(a/2)+1):a]
        rast[,(ceiling(a/2)+1):a]=rasti[,1:floor(a/2)]
    }
    
    return(rast)
}
        
        
remove_outside_range <- function(mask) {
    lat_range=360-round(2*(lat_range+89.75)+1)
    lon_range=round(2*(lon_range+179.75)+1)-360
    
    mask1=mask
    
    mask=as.matrix(mask1)
    a=dim(mask)
    
    mask[1:lat_range[2],]=NA
    mask[lat_range[1]:a[1],]=NA
    mask[,1:lon_range[1]]=NA
    mask[,lon_range[2]:a[2]]=NA
    
    values(mask1)=mask
    return(mask1)
    
}
    
 
write_to_new_nc <- function(mask) {
    writeRaster(mask,filename_out,format="CDF",overwrite=TRUE)
    nc <- open.nc(filename_out, write=TRUE) 
    
    var.rename.nc(nc, 'layer', mask_varname) 
    var.rename.nc(nc, 'latitude', 'lat') 
    var.rename.nc(nc, 'longitude', 'lon') 
      
    att.put.nc(nc, mask_varname, "description", "NC_CHAR", data_description)
    att.put.nc(nc, mask_varname, "script name", "NC_CHAR", sript_name)
    att.put.nc(nc, mask_varname, "more information", "NC_CHAR", contact_descp)
    att.put.nc(nc, mask_varname, "Date created", "NC_CHAR", date_descp)  
	close.nc(nc)
}